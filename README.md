# Lista de exercicios

<ol>
    <li>Soma de dois números: Escreva um programa que recebe dois números como entrada e retorna a soma deles.</li>
    <li>Média de três números: Escreva um programa que calcula a média de três números fornecidos como entrada.</li>
    <li>Verificação de número par ou ímpar: Crie um programa que verifica se um número fornecido pelo usuário é par ou ímpar.</li>
    <li>Cálculo de fatorial: Escreva um programa que calcula o fatorial de um número fornecido pelo usuário.</li>
    <li>Identificação do maior de dois números: Escreva um programa que determina qual dos dois números fornecidos pelo usuário é o maior.</li>
    <li>Contagem regressiva: Escreva um programa que imprime uma contagem regressiva de 10 até 1.</li>
    <li>Verificação de palíndromo: Crie um programa que verifica se uma palavra fornecida pelo usuário é um palíndromo (ou seja, se ela é a mesma se lida de trás para frente).</li>
    <li>Conversão de temperatura: Escreva um programa que converte uma temperatura fornecida em graus Celsius para Fahrenheit.</li>
    <li>Identificação do maior de três números: Escreva um programa que determina qual dos três números fornecidos pelo usuário é o maior.</li>
    <li>Tabuada de multiplicação: Crie um programa que imprime a tabuada de multiplicação de um número fornecido pelo usuário.</li>
    <li>Verificação de número primo: Escreva um programa que verifica se um número fornecido pelo usuário é primo.</li>
    <li>Cálculo de média ponderada: Crie um programa que calcula a média ponderada de três notas, onde os pesos são fornecidos pelo usuário.</li>
    <li>Cálculo de juros simples: Escreva um programa que calcula o montante final após aplicar juros simples a um principal, com taxa de juros e período fornecidos pelo usuário.</li>
    <li>Contagem de vogais em uma frase: Crie um programa que conta quantas vogais há em uma frase fornecida pelo usuário.</li>
    <li>Verificação de número perfeito: Escreva um programa que verifica se um número fornecido pelo usuário é um número perfeito.</li>
    <li>Conversão de número binário para decimal: Crie um programa que converte um número binário fornecido pelo usuário para decimal.</li>
    <li>Identificação de números primos em um intervalo: Escreva um programa que identifica e imprime todos os números primos em um intervalo fornecido pelo usuário.</li>
    <li>Contagem de ocorrências de um caractere em uma string: Crie um programa que conta quantas vezes um caractere específico ocorre em uma string fornecida pelo usuário.</li>
    <li>Cálculo do volume de uma esfera: Escreva um programa que calcula o volume de uma esfera, dado o raio fornecido pelo usuário.</li>
    <li>Verificação de ano bissexto: Crie um programa que verifica se um ano fornecido pelo usuário é bissexto ou não.</li>
</ol>
